echo 26 > /sys/class/gpio/export

#echo in > /sys/class/gpio/gpio26/direction

count=0
echo Counter "push button". To exit, hold down the button for more then 3 seconds.

finish=0
while [ $finish -eq 0 ]
do
    button=$(cat /sys/class/gpio/gpio26/value)
    
    if [ -z $button ]
    then
	echo "Error! You haven't GPIO."
	break
    fi
    
    if [ $button -eq 0 ]
    then
    let count=$count+1-$button
    echo "Sum: $count"
    # Long pressing
    hold=0
    while [ $button -eq 0 ]
    do
        # Anti quenching
        sleep 0.2
        # Exit if 0.2 * hold >= 3 second
        let hold=$hold+1
        if [ $hold -ge 15 ]
        then
	finish=1
	break
        fi
        button=$(cat /sys/class/gpio/gpio26/value)
    done
    fi
done

if [ $finish -ne 0 ]
then
    echo 26 > /sys/class/gpio/unexport
fi
