RANGE=100
COUNT=7

game () {
    number=$((RANDOM % RANGE + 1))
    let count=$COUNT
    while [ $count -gt 0 ]
    do
	read -p "Guess number from 1 to $RANGE: " num
	if [ $num -eq $number ]; then
    	    echo You GUESS!!!
    	    break
	elif [ $num -gt $number ]; then 
	    echo your greater
	else
	    echo your less
	fi
	let count=$count-1
    done
    if [ $count -eq 0 ]; then
	echo Times is over. GAME OVER 
    fi
}
answer="N"
clear
until [ $answer = "Y" ]; do
    
    echo Prease, select from menu:
    select menu in GAME SET_MAX_TRY_COUNT SET_RANGE EXIT
    do
	case $REPLY in
	1) game; break ;;
	2) read -p "Please, set count maximum times try: " COUNT
	clear; break ;;
	3) read -p "Please, set maximum range: " RANGE
	clear; break ;;
	4) answer="Y"; break ;;
	*) echo WRONG SELECT
        ;;
	esac
    done
done
