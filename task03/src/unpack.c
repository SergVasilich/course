#include <stdio.h>
#include <stdlib.h>

int main() {
  char ch, d, cnt = 0;

  while (!feof(stdin)) {
    cnt = getc(stdin);
    if (cnt == -1) break;
    if (cnt < 0) ch = getc(stdin);
    d = (cnt > 0) - (cnt < 0);
    while (cnt != 0) {
      if (cnt > 0) ch = getc(stdin);
      printf("%c", ch);
      cnt -= d;
    }
  }

  return 0;
}
