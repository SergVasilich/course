#include <stdio.h>
#include <stdlib.h>

const int MAXCNT = 5;

struct Vec {
  int len;
  char c[MAXCNT];
  struct Vec *next;
};

struct Vec *head, *res;

struct Vec *new_buf(struct Vec *next) {
  struct Vec *vec = (struct Vec *) malloc(sizeof(struct Vec));
  vec->len = 0;
  vec->next = next;
  return vec;
}

void put(struct Vec **curr, char ch) {
  if (*curr != NULL && (*curr)->len < MAXCNT)
    (*curr)->c[(*curr)->len++] = ch;
  else {
    *curr = new_buf(*curr);
    (*curr)->len = 1;
    (*curr)->c[0] = ch;
  }
}

int get(struct Vec **curr, char *ch) {
  if (*curr == NULL) return 0;
  if ((*curr)->len > 0) {
    *ch = (*curr)->c[--(*curr)->len];
    return 1;
  }
  struct Vec *p = *curr;
  *curr = (*curr)->next;
  free(p);
  return get(curr, ch);
}

int main() {
  char ch, last, cnt = 0;

  // get data without eof
  while (!feof(stdin)) put(&head, getc(stdin));
  get(&head, &last);

  // compress data with RLE
  while (1) {
    if (get(&head, &ch)) {
      if (cnt == 127 || cnt == -128) {
        put(&res, cnt);
        cnt = 0;
      }
      if (cnt == 0) {
        cnt = 1;
        last = ch;
        put(&res, ch);
        continue;
      }
      if (cnt == 1) {
        if (last == ch) cnt = -2;
        else {
          cnt = 2;
          last = ch;
          put(&res, ch);
        }
        continue;
      }
      if (last == ch) {
        if (cnt > 2) {
          get(&res, &last);
          put(&res, cnt - 1);
          put(&res, ch);
          cnt = -1;
        }
        if (cnt == 2) {
          put(&res, ch);
          cnt += 2;
        }
        cnt -= 1;
      } else {
        if (cnt < 0) {
          put(&res, cnt);
          cnt = 1;
        } else
          cnt += 1;
        last = ch;
        put(&res, ch);
      }
    } else
      break;
  }
  if (cnt != 0) put(&res, cnt);
  
  // write compressed to stdin
  while (get(&res,&ch)) printf("%c",ch);
  
  return 0;
}
