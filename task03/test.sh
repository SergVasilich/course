make

RED="\033[0;31m"
GRN="\033[0;32m"
NRM="\033[0m"

rm -f unpack.*

for file in $(ls tests/)
do
name="${file%.*}"
ext="${file##*.}"
result=$(bin/pak <tests/$file | bin/upak > unpack.$ext; diff -s tests/$file unpack.$ext)
if [ "$result" = "Files tests/$file and unpack.$ext are identical" ]
then
echo "Test $name ${GRN}PASSED ${NRM}"
else
echo "Test $name ${RED}FAILED ${NRM}"
fi
done
rm -f unpack.*
