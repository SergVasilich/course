function dec {
    echo $(($1/16*10+$1 % 16))
}
bus=$(ls /dev/i2c*)
bus="${bus#*-}"

echo 26 > /sys/class/gpio/export

echo "Time & temperature by push button. To exit, hold down the button for more then 1 seconds."

finish=0
while [ $finish -eq 0 ]
do
    button=$(cat /sys/class/gpio/gpio26/value)

    if [ -z $button ]
    then
        echo "Error! You haven't GPIO."
    break
    fi

    if [ $button -eq 0 ]
    then
        # Long pressing
    hold=0
    while [ $button -eq 0 ]
    do
	    # Anti quenching
        sleep 0.1
        let hold=$hold+1
	    if [ $hold -gt 10 ]
	    then
    	finish=1
    	break
	    fi
	    button=$(cat /sys/class/gpio/gpio26/value)
    done
    if [ $finish -eq 0 ]
    then
        R=$(i2cget -y $bus 0x68 0x0f)
        let "R|=0x20"
        i2cset -y 1 0x68 0x0E $R
        T1=$(($(i2cget -y $bus 0x68 0x11)))
        T2=$(($(i2cget -y $bus 0x68 0x12)/64*25))
        if [ $T1 -gt 127 ]
        then
	T1=$((($T1-256)*100+$T2))
	T2=$((-$T1 % 100))
	T1=$(($T1/100))
	if [ $T1 -eq 0 ]
	then
	    T1="-0"
	fi
        fi
        s=$(dec `i2cget -y $bus 0x68 0x0`)
        m=$(dec `i2cget -y $bus 0x68 0x1`)
        h=$(dec `i2cget -y $bus 0x68 0x2`)
        D=$(dec `i2cget -y $bus 0x68 0x4`)
        M=$(dec `i2cget -y $bus 0x68 0x5`)
        Y=$(dec `i2cget -y $bus 0x68 0x6`)
        printf "Date: 20%02d-%02d-%02d %02d:%02d:%02d  " $Y $M $D $h $m $s
        echo "Temperature is $T1.$T2"
    fi
    fi
done

if [ $finish -ne 0 ]
then
    echo 26 > /sys/class/gpio/unexport
fi