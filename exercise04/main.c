#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const TOP=6;
int res1[TOP];
char *res2[TOP];

int main() {
  srand(time(NULL));

  while (1) {
    printf("%45sGAME BONES!\n", "");
    char name[30];
    printf("Please, enter your name (enter q for exit): ");
    scanf(" %[^\n]s",name);
    if (strlen(name) == 1 && (name[0] == 'q' || name[0] == 'Q'))
      break;
    time_t clk = time(NULL);
    int first = 1 + rand() % 6;
    int second = 1 + rand() % 6;
    char res[80];
    sprintf (res, "%30s: %d + %d = %2d  at %s", name, first, second, first + second, ctime(&clk));
    printf("%s", res);

    int ln = TOP-1;
    if (res1[ln] > 0)
      free(res2[ln]);
    for (; ln > 0 && res1[ln - 1] <= first + second; ln--) {
      res1[ln] = res1[ln - 1];
      res2[ln] = res2[ln - 1];
    }
    res1[ln] = first + second;
    res2[ln] = calloc(strlen(res + 1), sizeof(char));
    strcpy(res2[ln], res);
    printf("%45sTOP RESULTS\n", "");
    for (int i = 0; i < TOP && res1[i] > 0; i++) {
      printf(res2[i]);
    }
  }
  return 0;
}
